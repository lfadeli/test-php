<?php
require_once("db.php");
?>
<html>
<head>
<title>PHP PDO CRUD</title>
<style>
body{
    font-family:arial;
    letter-spacing:1px;
    line-height:20px;
}
.tbl-qa{
    width: 100%;
    font-size:0.9em;
    background-color: aquamarine;
}
.tbl-qa th.table-header {
    padding: 5px;
    text-align: center;
    padding:10px;
}
.tbl-qa .table-row td {
    padding:10px;
    background-color: #FDFDFD;
    vertical-align:top;
}
.button_link
{
    color:#FFF;
    text-decoration:none;
    background-color:#428a8e;
    padding:10px;
}
.tb2{
        text-align: center;
        font-family: Calibri;
        font-weight: bold;
        color: brown;
        font-size: 1.2em;
        text-decoration: unset;

}
.icoon{
        margin-top: 10px;
}
</style>
</head>
<body>
<?php	
	$pdo_statement = $pdo_conn->prepare("SELECT * FROM posts ");
	$pdo_statement->execute();
	$result = $pdo_statement->fetchAll();
?>
<div style="text-align:right;margin:20px 0px;"><a href="add.php" class="button_link"><img class="icoon" width="50px" height="30px" src="crud-icon/adds.png" title="Add New Record" style="vertical-align:bottom;" /> Create</a></div>
<table class="tbl-qa">
  <thead>
	<tr>
	  <th class="table-header">Title</th>
	  <th class="table-header" >Description</th>
	  <th class="table-header" >Date</th>
	  <th class="table-header">Actions</th>
	</tr>
  </thead>
  <tbody id="table-body">
	<?php
	if(!empty($result)) { 
		foreach($result as $row) {
	?>
	  <tr class="table-row">
		<td class="tb2"><?php echo $row["post_title"]; ?></td>
		<td class="tb2"><?php echo $row["description"]; ?></td>
		<td class="tb2"><?php echo $row["post_at"]; ?></td>
		<td class="tb2"><a class="ajax-action-links" href='edit.php?id=<?php echo $row['id']; ?>'><img width="60px" height="30px" src="crud-icon/edites.png" title="Edit" /></a> <a class="ajax-action-links" href='delete.php?id=<?php echo $row['id']; ?>'><img width="60px" height="30px" src="crud-icon/deletes.png" title="Delete" /></a></td>
	  </tr>
    <?php
		}
	}
	?>
  </tbody>
</table>
<?php
var_dump($result);
?>
<h1>hello lfadeli soufiane</h1>
</body>
</html>